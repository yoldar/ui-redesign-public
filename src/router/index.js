import VueRouter from 'vue-router'
import Home from '@/components/Home.vue'
import Calculator from '@/components/Calculator.vue'
import PreliminaryCheck from '@/components/PreliminaryCheck.vue'
import QrPayment from '@/components/QrPayment.vue'
import FinalCheck from '@/components/FinalCheck.vue'
import CheckPosition from '@/components/CheckPosition.vue'
import CashRegister from '@/components/CashRegister.vue'
import CashRegisterStep2 from '@/components/CashRegisterStep2.vue'
import CashRegisterStep3 from '@/components/CashRegisterStep3.vue'
import CashRegisterStep4 from '@/components/CashRegisterStep4.vue'
import CashRegisterStep5 from '@/components/CashRegisterStep5.vue'
import LoginStep1 from '@/components/LoginStep1.vue'
import LoginStep2 from '@/components/LoginStep2.vue'

const routes = [
    { path: '/', component: Home, meta: {
      hasTopNav: true,
      hasBottomNav: true,
    } },
    { path: '/calculator', component: Calculator, meta: {
      hasTopNav: true,
      hasBottomNav: true,
    } },
    { path: '/preliminary-check', component: PreliminaryCheck, meta: {
      hasTopNav: true,
      hasBottomNav: false,
    }},
    { path: '/qr-payment', component: QrPayment, meta: {
      hasTopNav: false,
      hasBottomNav: false,
    }},
    { path: '/final-check', component: FinalCheck, meta: {
      hasTopNav: false,
      hasBottomNav: false,
    }},
    {
      path: '/check-positions', component: CheckPosition, meta: {
        hasTopNav: true,
        hasBottomNav: false
      }
    },
    {
      path: '/cash-register', component: CashRegister, meta: {
        hasTopNav: true,
        hasBottomNav: false
      }
    },
    {
      path: '/cash-register-step2', component: CashRegisterStep2, meta: {
        hasTopNav: true,
        hasBottomNav: false
      }
    },
    {
      path: '/cash-register-step3', component: CashRegisterStep3, meta: {
        hasTopNav: true,
        hasBottomNav: false
      }
    },
    {
      path: '/cash-register-step4', component: CashRegisterStep4, meta: {
        hasTopNav: true,
        hasBottomNav: false
      }
    },
    {
      path: '/cash-register-step5', component: CashRegisterStep5, meta: {
        hasTopNav: true,
        hasBottomNav: false
      }
    },
    {
      path: '/login-step1', component: LoginStep1, meta: {
        hasTopNav: false,
        hasBottomNav: false
      }
    },
    {
      path: '/login-step2', component: LoginStep2, meta: {
        hasTopNav: false,
        hasBottomNav: false
      }
    }
]

const router = new VueRouter({
    mode: 'history',
    routes,
  });
  
export default router;