import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import ru from 'vuetify/lib/locale/ru';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
      dark: true,
      options: {
        customProperties: true,
      },
      themes: {
        light: {
          primary: '#007BFF',
          secondary: '#424242',
          accent: '#82B1FF',
          error: '#FF5252',
          info: '#2196F3',
          success: '#4CAF50',
          warning: '#FFC107'
        },
        dark: {
          secondary: '#83B2BD',
          primary: '#356A75',
          primaryDarken1: '#19454E',
          primaryDarken2: '#002D2D',
          background: '#26545D',
          error: '#FF6464',
          anchor: '#B6CDD2',
          info: '#15BAAA',
        }
      },
    },
      lang: {
        locales: { ru },
        current: 'ru',
      },
  });
